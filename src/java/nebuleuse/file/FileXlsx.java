/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nebuleuse.file;

import java.awt.Color;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/*
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
*/

import org.apache.poi.ss.usermodel.Cell; 
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row; 
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook;






/**
 *
 * @author hugo
 */
public class FileXlsx extends FileBin {


    
    
    private XSSFWorkbook libro;
    private XSSFSheet hoja;
    private Row fila;
    private ArrayList <String> cabecera =  new ArrayList<>();
    private ArrayList <String> campos =  new ArrayList<>();
    private java.awt.Color color = new java.awt.Color(85, 139, 47);       
    private java.awt.Color colorText = new java.awt.Color(255,255,255);
    //private java.awt.Color colorText = new java.awt.Color(255,255,255);
       
      
    
    
        
    public  void gen ( ResultSet resulset ) throws SQLException, Exception {
        
           
            this.newlibro();
            //this.newhoja("hoja1");


            this.writeCabecera(0);
            this.writeContenido(resulset);
            
        
        // Se salva el libro.
        try {

                String ruta = "";
                ruta = this.getFilePath();

                
                FileOutputStream file = new FileOutputStream( ruta );                
                
                this.getLibro().write(file);

                file.close();                
            
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            
        }
    }
    
    
    
    
    
    public  void newlibro () {        
        this.setLibro(new XSSFWorkbook());        
    }    
    

    public  void newhoja (String nombrehoja) {        
        this.hoja = this.getLibro().createSheet(nombrehoja);
    }    
    
    
    public  void newfila (Integer indice) {        
        this.setFila(this.hoja.createRow(indice));
    }        
    
    
    
    
    public  void writeCabecera (Integer indice) {        
        
        this.newfila(0);

        int i = 0;
        for (String titulo : cabecera) {     
            
            
            Cell cell = this.getFila().createCell(i);
            cell.setCellValue(titulo);
            
            //this.getFila().createCell(i).setCellValue( titulo );   
            
            i++;
        }
    }            

    

    
    
    public  void writeText (Integer indice) {        
        
        this.newfila(indice);

        int i = 0;
        for (String titulo : cabecera) {     
            
            
            Cell cell = this.getFila().createCell(i);
            cell.setCellValue(titulo);
            //this.getFila().createCell(i).setCellValue( titulo );               
            i++;
        }
    }            

    

    
    
    
    
    public  void writeContenido ( ResultSet resulset ) throws SQLException {        

        Integer nrofila = 1;        

        while (resulset.next()) {            
            //String em = resulset.getString("EM_ID");
            int i = 0;
            
            this.newfila(nrofila);
            
            for (String campo : this.campos) {   
                
                String em = "";                

                if (resulset.getString(campo) != null){
                    em = resulset.getString(campo);                    
                    String cad = em;
                    
                    
                    try {
                        Long num = Long.parseLong(cad);
                        this.getFila().createCell(i).setCellValue(Long.parseLong(cad));
                    }
                    catch (NumberFormatException nfe) {
                        this.getFila().createCell(i).setCellValue( em );
                    }                    
                    
         /*           
                    //if (cad.matches("[0-9]*")){
                    if ( Long.parseLong(cad) ){
                        
                    }
                    else{                        
                    }  
*/
                }
                
              i++;
            }            
            nrofila++;
        }        
                 

    }            

    


    
    public  Integer writeContenido ( ResultSet resulset, Integer nrofila ) throws SQLException {        


        while (resulset.next()) {            
            
            int i = 0;            
            this.newfila(nrofila);
            
            for (String campo : this.campos) {   
                
                String em = "";                

                if (resulset.getString(campo) != null){
                    em = resulset.getString(campo);                    
                    String cad = em;
                    
                    
                    try {
                        Long num = Long.parseLong(cad);
                        this.getFila().createCell(i).setCellValue(Long.parseLong(cad));
                    }
                    catch (NumberFormatException nfe) {
                        this.getFila().createCell(i).setCellValue( em );
                    }                    
                    
         /*           
                    //if (cad.matches("[0-9]*")){
                    if ( Long.parseLong(cad) ){
                        
                    }
                    else{
                        
                    }  
*/
                    
                    
                }

          
                
                i++;
            }            
            nrofila++;
        }        
                 
        return nrofila;
    }            

    
    

    

    public  void gen (Integer indice) {        
        
        this.newfila(0);

        int i = 0;
        for (String titulo : cabecera) {        
            this.getFila().createCell(i).setCellValue( titulo );
            i++;
        }
    }            
    
    
    
    
    
    
    
    
    public ArrayList <String> getCabecera() {
        return cabecera;
    }

    public void setCabecera(ArrayList <String> cabecera) {
        this.cabecera = cabecera;        
    }

    public ArrayList <String> getCampos() {
        return campos;
    }

    public void setCampos(ArrayList <String> campos) {
        this.campos = campos;
    }

    public XSSFWorkbook getLibro() {
        return libro;
    }

    public void setLibro(XSSFWorkbook libro) {
        this.libro = libro;
    }
    
    public Row getFila() {
        return fila;
    }

    public void setFila(Row fila) {
        this.fila = fila;
    }    
    
    public XSSFSheet getHoja() {
        return hoja;
    }

    

    

    
    public  void formato () {    
        this.formato(this.color, 0);
    }            


    
    

    
    public  void formato (java.awt.Color color, Integer line) {        

        
        int i = 0;
        for (String titulo : this.getCabecera()) {  
            
            Cell cell = this.getHoja().getRow(line).getCell(i);
            
            XSSFCellStyle cellStyle = this.getHoja().getWorkbook().createCellStyle();            
            
            //java.awt.Color color = new java.awt.Color(220, 220, 220);            
            cellStyle.setFillForegroundColor(new XSSFColor(color, new DefaultIndexedColorMap()));            
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            
            
            //XSSFFont  font = this.getLibro().createFont();
/*            
            XSSFFont   font = new XSSFFont();            
            font = cellStyle.getFont();
            font.setColor(new XSSFColor(
                    this.colorText, 
                    new DefaultIndexedColorMap()));
            cellStyle.setFont(font);
  */                      
            cell.setCellStyle(cellStyle);
            
            i++;
        }        
        
        
        
    }            

    public java.awt.Color getColor() {
        return color;
    }

    public void setColor(java.awt.Color color) {
        this.color = color;
    }

    
    
    
}        