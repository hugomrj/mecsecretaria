/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento_movimiento;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import py.com.aplicacion.oficina_interna.OficinaInterna;



/**
 *
 * @author hugom_000
 */

public class DocumentoMovimientoDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public DocumentoMovimientoDAO ( ) throws IOException  {
    }
      
    
        

    public List<DocumentoMovimiento>  sublista (Integer orden) throws Exception {
                
        
        String sql = "";
        sql = sql + new DocumentoMovimientoSQL().sublista(orden);     
        
        
        List<DocumentoMovimiento>  lista = null;        
        try {    
            
            ResultadoSet rs = new ResultadoSet();      
            
            lista = new Coleccion<DocumentoMovimiento>().resultsetToList(
                    new DocumentoMovimiento(),
                    rs.resultset(sql)
            );                        
                  
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
     
    
    
    
    
    
    public void addPrimerRegistro(Integer documento, 
            Date fecha, Integer oficina) throws Exception{
    
        DocumentoMovimiento movimiento = new DocumentoMovimiento();
        
        movimiento.setDocumento(documento);
        movimiento.setFecha(fecha);
        movimiento.setObservacion("");
        
        OficinaInterna ofi = new OficinaInterna();
        ofi.setOficina_interna(oficina);
        movimiento.setOficina_interna(ofi);
        
        persistencia.insert(movimiento);          
        
    
    }
    
    

    
}
