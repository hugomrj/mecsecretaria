/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.aplicacion.documento_movimiento;

import java.util.Date;
import py.com.aplicacion.oficina_interna.OficinaInterna;

/**
 *
 * @author hugo
 */
public class DocumentoMovimiento {
    
    private Integer id;
    private Integer documento;
    private Date fecha;
    private OficinaInterna oficina_interna;
    private String observacion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDocumento() {
        return documento;
    }

    public void setDocumento(Integer documento) {
        this.documento = documento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public OficinaInterna getOficina_interna() {
        return oficina_interna;
    }

    public void setOficina_interna(OficinaInterna oficina_interna) {
        this.oficina_interna = oficina_interna;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
