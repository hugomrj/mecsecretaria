/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento_movimiento;


import nebuleuse.ORM.sql.ReaderT;

/**
 *
 * @author hugo
 */
public class DocumentoMovimientoSQL {    
    
    
    
    public String sublista (Integer orden )
            throws Exception {
    
        String sql = "";                                 
        
        
        ReaderT reader = new ReaderT("DocumentoMovimiento");
        reader.fileExt = "sublista.sql";
        
        sql = reader.get( orden );    
        
        return sql ;             
    
    }   
    
    
    
}
