/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento_digitalizacion;



import java.io.IOException;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugo
 */
public class DocumentoDigitalizacionSQL {
    
    
        
     
    public String insert ( String file_name, String  file_id, Integer documento )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("DocumentoDigitalizacion");
        reader.fileExt = "insert.sql";
        
        sql = reader.get( file_name, file_id, documento );    
        
        return sql ;             
    
    }   
       
    
 
     
    public String delete ( Integer documento ) throws IOException {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("DocumentoDigitalizacion");
        reader.fileExt = "delete.sql";
        
        sql = reader.get( documento );    
        
        return sql ;             
    
    }   

    
    public String filterFile ( Integer documento )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("DocumentoDigitalizacion");
        reader.fileExt = "filterFile.sql";
        
        sql = reader.get( documento );    
        
        return sql ;             
    
    }   
               
    
    
}
