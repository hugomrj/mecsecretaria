/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento_digitalizacion;


import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.xml.Global;



/**
 *
 * @author hugom_000
 */

public class DocumentoDigitalizacionDAO  {

    
    
    public DocumentoDigitalizacionDAO () throws IOException  {
    }
    
    


    public Integer insert ( String file_name, String file_id, 
            Integer documento ) 
            throws SQLException, Exception {
        
        Integer intID = 0;

        
        String strSQL = new DocumentoDigitalizacionSQL()
                .insert(file_name, file_id, documento) ;
        
        Persistencia persinstencia = new Persistencia();
        
        intID = persinstencia.ejecutarSQL(strSQL, "id" );

        
        return intID;

    }
    
    
    
    

    public void delete ( Integer documento ) 
            throws SQLException, Exception {
        
        Integer intID = 0;
        
        
        String strSQL = new DocumentoDigitalizacionSQL()
                .delete(documento);

        Persistencia persinstencia = new Persistencia();
        
        persinstencia.ejecutarSQL(strSQL);

    }
    



    public DocumentoDigitalizacion filterFile ( Integer documento ) 
            throws SQLException, Exception {

        
        DocumentoDigitalizacion obj  = new DocumentoDigitalizacion();

        
        String strSQL = new DocumentoDigitalizacionSQL()
                .filterFile(documento);
        
        Persistencia persinstencia = new Persistencia();        
        obj = (DocumentoDigitalizacion) persinstencia.sqlToObject(strSQL, obj);
        
        return obj;

    }
    
    


    public void deleteFile ( Integer documento ) 
            throws SQLException {
        
        try {
            
            
            DocumentoDigitalizacion obj = this.filterFile(documento);            
            
            if (obj != null)
            {   
                String dir_files = new Global().getValue("dir_files");            
                
                File file = new File( dir_files + obj.getFile_id());                        
                
                if (file.exists()) {                                                         
                    file.delete();                    
                }
            }
        }         
        catch (Exception ex) 
        {
            System.err.println(ex.getMessage());
        }

    }
    
    
    
    
    
          
        
}
