/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.oficina_interna;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;



/**
 *
 * @author hugom_000
 */

public class OficinaInternaDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public OficinaInternaDAO ( ) throws IOException  {
    }
      
    


    public List<OficinaInterna>  all () throws Exception {
        
        String sql = new OficinaInternaSQL().all();
        
                
        List<OficinaInterna>  lista = null;        
        try {                        
                        
            ResultadoSet rs = new ResultadoSet();    
            
            lista = new Coleccion<OficinaInterna>().resultsetToList(
                    new OficinaInterna(),
                    rs.resultset(sql)
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
        
    
    
}
