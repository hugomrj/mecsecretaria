/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.aplicacion.oficina_interna;

/**
 *
 * @author hugo
 */
public class OficinaInterna {
    
    private Integer oficina_interna;
    private String descripcion;

    public Integer getOficina_interna() {
        return oficina_interna;
    }

    public void setOficina_interna(Integer oficina_interna) {
        this.oficina_interna = oficina_interna;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


}
