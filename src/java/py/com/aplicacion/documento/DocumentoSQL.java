/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento;


import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugo
 */
public class DocumentoSQL {
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select( new Documento(), busqueda );        
        
        return sql ;             
    }        
           
    
        
     
    public String filtro (String buscar )
            throws Exception {
    
        String sql = "";                                 
        
        if (buscar != null) {
            buscar = buscar.replace(" ", "%") ;    
        }        
        
        ReaderT reader = new ReaderT("Documento");
        reader.fileExt = "filtro.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;             
    
    }   
       
    
    
}
