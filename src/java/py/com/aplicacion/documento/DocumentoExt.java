/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import py.com.aplicacion.documento_digitalizacion.DocumentoDigitalizacion;
import py.com.aplicacion.documento_digitalizacion.DocumentoDigitalizacionDAO;
import py.com.aplicacion.documento_movimiento.DocumentoMovimiento;
import py.com.aplicacion.documento_movimiento.DocumentoMovimientoDAO;
import py.com.aplicacion.documento_salida.DocumentoSalida;
import py.com.aplicacion.documento_salida.DocumentoSalidaDAO;

/**
 *
 * @author hugo
 */

public class DocumentoExt extends Documento{
    
    
    private List<DocumentoMovimiento> moviemientos = new ArrayList <DocumentoMovimiento>() ;   
    private DocumentoSalida salida = new DocumentoSalida();
        
    private DocumentoDigitalizacion documento_digitalizadion;
    
    
    public void extender() throws IOException, Exception {
                     
        //Persistencia persistencia = new Persistencia(); 
        
        
        DocumentoMovimientoDAO movimientoDAO = new DocumentoMovimientoDAO();
        this.moviemientos = movimientoDAO.sublista(this.getDocumento() );
        
        
        DocumentoSalidaDAO salidaDAO = new DocumentoSalidaDAO();
        this.salida = salidaDAO.filtrarDocumento( this.getDocumento());
        
        
        
             
        /*
        String strSQL = new DocumentoDigitalizacionSQL().filterFile(this.getDocumento());
        
        
        this.documento_digitalizadion = new DocumentoDigitalizacion();           
        this.documento_digitalizadion  = (DocumentoDigitalizacion) 
                persistencia.sqlToObject(strSQL, this.documento_digitalizadion);              
        */
        
        this.documento_digitalizadion  = new  DocumentoDigitalizacionDAO().filterFile(this.getDocumento());
        
    }


    
     
            
}
