/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento;


import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.util.Enumeration;
import nebuleuse.ORM.xml.Global;
import py.com.aplicacion.documento_digitalizacion.DocumentoDigitalizacionDAO;



@WebServlet(
        name = "GestionSocialUpload", 
        urlPatterns = { "/gestionsocial/upload" }
)


@MultipartConfig(
  fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
  maxFileSize = 1024 * 1024 * 10,      // 10 MB
  maxRequestSize = 1024 * 1024 * 100   // 100 MB
)


public class DocumentoUpload extends HttpServlet {

  public void doPost(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
      
         
      
      
      try {
         
          
          
          Integer documento = 0;
          Enumeration<String> names = request.getHeaderNames();
          while (names.hasMoreElements()) {
              String headerName = names.nextElement();
              if (headerName.equals("documento")){
                  documento =  Integer.parseInt(request.getHeader(headerName));
              }
          }
          
          
          String dir_files = new Global().getValue("dir_files");
   

          
          long now = System.currentTimeMillis();
          String file_id = String.valueOf(now);
                    
          /* Receive file uploaded to the Servlet from the HTML5 form */
          Part filePart = request.getPart("file");
          String fileName = filePart.getSubmittedFileName();
          
          
          
          for (Part part : request.getParts()) {
              //part.write("/home/host/archivos/" + fileName);
              //part.write( dir_files + fileName);
              part.write( dir_files + file_id);              
          }
                    
          
          
          DocumentoDigitalizacionDAO digitalDAO = new DocumentoDigitalizacionDAO();          
          
          digitalDAO.deleteFile(documento);
          digitalDAO.delete(documento);          
          digitalDAO.insert(fileName, file_id, documento );
                              
          
      } 
      
      /*
      catch (SQLException ex) {
          System.out.println(ex.getMessage());
      }
*/
      catch (Exception ex) {
          System.out.println(ex.getMessage());
      }
  }

}    
    
    

