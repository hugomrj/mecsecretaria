/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.aplicacion.documento;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import py.com.aplicacion.documento_digitalizacion.DocumentoDigitalizacionDAO;
import py.com.aplicacion.documento_movimiento.DocumentoMovimientoDAO;
import py.com.base.sistema.usuario.UsuarioExt;



/**
 * REST Web Service
 * @author hugo
 */


@Path("documentos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class DocumentoWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    private Response.Status status  = Response.Status.OK;
    
    String json = "";
    
    Documento com = new Documento();       
                         
    public DocumentoWS() {
    }

        
    
    @GET    
    public Response lista ( 
            @HeaderParam("token") String strToken,
            @MatrixParam("q") String q,
            @MatrixParam("aa") Integer aa,
            @QueryParam("page") Integer page) {
        
        
            if (page == null) {                
                page = 1;
            }

            
            if (aa == null) {                
                aa = 0;
            }
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();         
                
                
                JsonObject jsonObject ;                                                
                if (q == null) {                                
                    jsonObject = new DocumentoJSON().lista(page, null, aa);
                } 
                else{
                    jsonObject = new DocumentoJSON().lista(page, q, aa);
                }                
                
                //JsonObject jsonObject = new DocumentoJSON().lista(page);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else
            {
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                                             
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
      
 
    
    
    
    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                     
        try 
        {                  
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();        
                
                DocumentoExt ext = new DocumentoExt();
                
                ext = (DocumentoExt) persistencia.filtrarId(ext, id );  
                
                String json = gson.toJson(ext);
                
                if (ext == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                return Response
                        .status( this.status )                        
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }
        
        }     
        catch (Exception ex) {
           
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")                    
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        
        
    }    
      
        
        


 
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
   

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                Documento req = gson.fromJson(json, Documento.class);                   
                
                
                /*
                Calendar calendar = new GregorianCalendar();
                calendar.setTime(req.getFecha_ingreso());
                req.setAgno(  calendar.get(Calendar.YEAR));
                */
                
                
                this.com = (Documento) persistencia.insert(req);           
                                                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }
                else {
                
                    UsuarioExt usuarioex = new UsuarioExt();
                    Integer codofi = usuarioex.getOficina(autorizacion.token.getUser());
                    
                    // primar campo de detalle
                    new DocumentoMovimientoDAO().addPrimerRegistro(
                            this.com.getDocumento(), 
                            this.com.getFecha_ingreso(), 
                            codofi);
                
                
                }
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(this.status)
                        .entity( json )
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else
            {                 
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();             
            }
        
        }     
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
    
        
    
         
         

    @PUT    
    @Path("/{id}")    
    public Response edit (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id,
            String json  ) {

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                Documento req = new Gson().fromJson(json, Documento.class);                                                      
                req.setDocumento(id);
                
                this.com = (Documento) persistencia.update(req);
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
            
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                    
    
        }        
    }    
    
    
    
    
    

    @DELETE  
    @Path("/{id}")    
    public Response delete (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
            
                Integer filas = 0;
                filas = persistencia.delete(this.com, id) ;      
                
                
                DocumentoDigitalizacionDAO digital = new DocumentoDigitalizacionDAO();  
                digital.deleteFile(id);
                digital.delete(id);                        
                
                                
                
                
                if (filas != 0){
                    
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{                    
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();          
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }  
        
    }    
        
      

    
    @GET       
    //@Path("/search/{opt : (.*)}") 
    @Path("/search/") 
    public Response search ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,              
            @MatrixParam("q") String q
            ) {
        
        
            if (page == null) {                
                page = 1;
            }
            if (q == null){            
                q = "";                
            }
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();               
                
                JsonObject jsonObject = new DocumentoJSON().search(q, page);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();        
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
        
    


    
    
}