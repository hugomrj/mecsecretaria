/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.aplicacion.documento;

import java.util.Date;

/**
 *
 * @author hugo
 */
public class Documento {
    
    private Integer documento;
    private Date fecha_ingreso;
    private String procedencia;
    private String expediente_numero;
    private String referencia;
    
    
    
    public Integer getDocumento() {
        return documento;
    }

    public void setDocumento(Integer documento) {
        this.documento = documento;
    }

    public Date getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(Date fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getExpediente_numero() {
        return expediente_numero;
    }

    public void setExpediente_numero(String expediente_numero) {
        this.expediente_numero = expediente_numero;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }
    
}

