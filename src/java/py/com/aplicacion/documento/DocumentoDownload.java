/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.xml.Global;
import nebuleuse.util.Download;
import py.com.aplicacion.documento_digitalizacion.DocumentoDigitalizacion;
import py.com.aplicacion.documento_digitalizacion.DocumentoDigitalizacionDAO;

/*
import py.com.base.aplicacion.proyectovigentedocumento.ProyectoVigenteDocumento;
import py.com.base.aplicacion.proyectovigentedocumento.ProyectoVigenteDocumentoDAO;
*/


@WebServlet(
        name = "DocumentoDownload", 
        urlPatterns = { "/Documento/Download" }
)



public class DocumentoDownload extends HttpServlet {

    

@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    
    
System.out.println("docuemento download");
    
    try {
        
        //downloadFile(resp,   this.getServletContext().getRealPath("/
                
        String dir_files = new Global().getValue("dir_files");
        
        // obtener RequestHeader
        Integer documento = Integer.parseInt(req.getHeader("documento"));

System.out.println("documento");        
System.out.println(documento);        
        
        //DocumentoDigitalizacionDAO doc = new DocumentoDigitalizacionDAO().filterProyecto(regid);
        DocumentoDigitalizacion digital = new DocumentoDigitalizacionDAO().filterFile(documento);
        
        Download down = new Download();
        down.downloadFile(resp, dir_files+digital.getFile_id(), digital.getFile_name() );
        
        
    } catch (Exception ex) {
        Logger.getLogger(DocumentoDownload.class.getName()).log(Level.SEVERE, null, ex);
    }
        
    
}    
 
    
    

}    
    
    

