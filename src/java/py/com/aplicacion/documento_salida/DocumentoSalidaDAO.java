/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento_salida;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.Calendar;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;



/**
 *
 * @author hugom_000
 */

public class DocumentoSalidaDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public DocumentoSalidaDAO ( ) throws IOException  {
    }
      
    
    
    
    public DocumentoSalida  filtrarDocumento (Integer documento) throws Exception {      

          DocumentoSalida obj = new DocumentoSalida();  

          String sql = new DocumentoSalidaSQL().filtrarDocumento(documento);
                
          obj = (DocumentoSalida) persistencia.sqlToObject(sql, obj);

          return obj;          

      }
     

    public Integer ultimoNumeroMemo (DocumentoSalida documentosalida) throws Exception {
    
        Integer ret = 0;

        
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(documentosalida.getFecha_salida());        
        int year = calendar.get(Calendar.YEAR);

        
        String sql = new DocumentoSalidaSQL().ultimoNumeroMemo( year );
        
        ResultadoSet resSet = new ResultadoSet();   
        ResultSet rsData = resSet.resultset(sql);      
        
        
        if (rsData.next()){
            ret =  Integer.parseInt(rsData.getString("memo_numero"));
        }             
            
        
        return ret;
    }
    
    
}
