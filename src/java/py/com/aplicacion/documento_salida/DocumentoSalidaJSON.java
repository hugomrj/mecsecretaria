/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento_salida;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;


public class DocumentoSalidaJSON  {


    
    
    public DocumentoSalidaJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonObject  lista ( Integer page, String buscar, Integer agno) {
        
        
        JsonObject jsonObject = new JsonObject();
        
        
        try 
        {   
            ResultadoSet resSet = new ResultadoSet();    

            String sql = "";
            String sqlFiltro = "";
            String sqlOrder = "";
            String sqlAAAA = "";            
            
            
            if (buscar == null) {                
                sql = new DocumentoSalidaSQL().lista();    
            }
            else{
                
                sql = new DocumentoSalidaSQL().lista();    
                sqlFiltro =  new DocumentoSalidaSQL().filtro(buscar);   ;   
    
            }   
            
            if ( agno != 0 ){  
                sqlAAAA = " extract(year from fecha_ingreso)  = " + agno ;                
                if (sqlFiltro.equals("")) {                
                    sqlAAAA = " where " + sqlAAAA;    
                }
                else{                
                    sqlAAAA = " and " + sqlAAAA;    
                }            
            }
                      
            sqlOrder = " order by documentos.documento desc   ";

            sql = sql + sqlFiltro + sqlAAAA + sqlOrder ;
         
            
            
            
            ResultSet rsData = resSet.resultset(sql, page);                
            
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
    
    /*

    public JsonObject  search ( String query, Integer page) {
        
        
        JsonObject jsonObject = new JsonObject();

        
        try 
        {   
              

            ResultadoSet resSet = new ResultadoSet();                   
            String sql = new DocumentoSQL().search(query);          
            

            ResultSet rsData = resSet.resultset(sql, page);                
            
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    */
    
        
        
    
    
    
        
}
