/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.documento_salida;



import nebuleuse.ORM.sql.ReaderT;

/**
 *
 * @author hugo
 */
public class DocumentoSalidaSQL {    
    
    
    
    public String lista ()
            throws Exception {
    
        String sql = "";                                 
        
        
        ReaderT reader = new ReaderT("DocumentoSalida");
        reader.fileExt = "lista.sql";
        
        sql = reader.get( );    
        
        
        return sql ;             
    
    }   


    
    

    public String filtrarDocumento (Integer documento)
            throws Exception {
    
        String sql = "";                                 
                
        ReaderT reader = new ReaderT("DocumentoSalida");
        reader.fileExt = "filtrarDocumento.sql";
        
        sql = reader.get(documento );    
        
        return sql ;             
    
    }   
    
    

    public String ultimoNumeroMemo (int year)
            throws Exception {
    
        String sql = "";   
        
        ReaderT reader = new ReaderT("DocumentoSalida");
        reader.fileExt = "ultimoNumeroMemo.sql";
        
        sql = reader.get(year);    
        
        return sql ;             
    
    }       
    
    
        
     
    public String filtro (String buscar )
            throws Exception {
    
        String sql = "";                                 
        
        if (buscar != null) {
            buscar = buscar.replace(" ", "%") ;    
        }        
        
        ReaderT reader = new ReaderT("DocumentoSalida");
        reader.fileExt = "filtro.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;             
    
    }   
         
    
    
    
}
