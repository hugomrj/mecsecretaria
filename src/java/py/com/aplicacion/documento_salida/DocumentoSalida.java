/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.aplicacion.documento_salida;


import java.util.Date;

/**
 *
 * @author hugo
 */
public class DocumentoSalida {
    
    private Integer id;
    private Date fecha_salida;    
    private Integer memo_numero;
    private String destino;
    private String archivadoen;
    private Integer documento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha_salida() {
        return fecha_salida;
    }

    public void setFecha_salida(Date fecha_salida) {
        this.fecha_salida = fecha_salida;
    }


    public Integer getMemo_numero() {
        return memo_numero;
    }

    public void setMemo_numero(Integer memo_numero) {
        this.memo_numero = memo_numero;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getArchivadoen() {
        return archivadoen;
    }

    public void setArchivadoen(String archivadoen) {
        this.archivadoen = archivadoen;
    }

    public Integer getDocumento() {
        return documento;
    }

    public void setDocumento(Integer documento) {
        this.documento = documento;
    }
        
    
    
}

