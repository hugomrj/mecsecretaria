



function main_form (obj) {    


    fetch( html.url.absolute() + '/consulta/servid/htmf/cab.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('arti_form').innerHTML = data ;
        
            main_lista(obj);
            
      })

};







function main_lista (obj) {    

    fetch( html.url.absolute() + '/consulta/servid/htmf/det.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('main_lista').innerHTML = data ;
        
        form_ini (obj);
            
      })

};






function form_ini (obj) {    

    // boton enviar
    var btn_cab_buscar = document.getElementById('btn_cab_buscar');
    btn_cab_buscar.onclick = function(event) {     

        var fecha_desde = document.getElementById('fecha_desde');
        var fecha_hasta = document.getElementById('fecha_hasta');

        if (fecha_desde.value != "" &&  fecha_hasta.value != "" ){        
            Consulta_detalle_promesa( obj  );
        }
        
    }
    

    
    
    
    

    var btn_xlsx = document.getElementById( 'btn_xlsx');
    btn_xlsx.onclick = function()
    {  


        var fecha_desde = document.getElementById('fecha_desde');
        var fecha_hasta = document.getElementById('fecha_hasta');

        if (fecha_desde.value != "" &&  fecha_hasta.value != "" ){        

            loader.inicio();

            var url = html.url.absolute() + '/api/ordenesservicios_qry/consulta1/xlsx/'                
                +';fe1='+document.getElementById("fecha_desde").value 
                +';fe2='+document.getElementById("fecha_hasta").value 


            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {

                if (xhttp.readyState === 4 && xhttp.status === 200) {

                    var file_name = "ordenservicio_detalle.xlsx" ; 

                    // Trick for making downloadable link
                    var a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    a.download = file_name;
                    a.click();
                    loader.fin();

                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.setRequestHeader("token", localStorage.getItem('token'));           
            xhttp.send();

        }
        
        

    }

            
    
          
};







function Consulta_detalle_promesa( obj ) {    


    const promise = new Promise((resolve, reject) => {

            loader.inicio();            

            var url = html.url.absolute() + '/api/ordenesservicios_qry/consulta1/'                
                +';fe1='+document.getElementById("fecha_desde").value 
                +';fe2='+document.getElementById("fecha_hasta").value 
                

            var xhr =  new XMLHttpRequest();      

            var metodo = "GET";                                     
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    tabla.json = xhr.responseText;


                    var ojson = JSON.parse( tabla.json ) ; 
                    tabla.json = JSON.stringify(ojson['datos']) ;  
                    
                   
                    if (tabla.json != "[]"){
                    
                        tabla.ini(obj);                            
                        tabla.gene();                                  
                        tabla.formato(obj);                            
                                                
                        
                        // suma de consultas
                        var jsumas = JSON.stringify(ojson['summary'][0]) ;              
                        var ojsumas = JSON.parse(jsumas) ;

                        
                        document.getElementById("sillas").innerHTML
                            =  fmtNum( ojsumas["sillas"] );
                        
                        document.getElementById("mesas").innerHTML
                            =  fmtNum( ojsumas["mesas"] );
                        
                        document.getElementById("refrigerios").innerHTML
                            =  fmtNum( ojsumas["refrigerios"] );
                        
                        document.getElementById("microfono_parlante").innerHTML
                            =  fmtNum( ojsumas["microfono_parlante"] );
                        
                        document.getElementById("vehiculos").innerHTML
                            =  fmtNum( ojsumas["vehiculos"] );
                        
                        document.getElementById("proyector").innerHTML
                            =  fmtNum( ojsumas["proyector"] );
                        
                        document.getElementById("computadora").innerHTML
                            =  fmtNum( ojsumas["computadora"] );
                        
                        document.getElementById("materiales_impresos").innerHTML
                            =  fmtNum( ojsumas["materiales_impresos"] );
                        
                        document.getElementById("boligrafos").innerHTML
                            =  fmtNum( ojsumas["boligrafos"] );
                        
                        document.getElementById("hojas_blancas").innerHTML
                            =  fmtNum( ojsumas["hojas_blancas"] );
                        
                        document.getElementById("otros").innerHTML
                            =  fmtNum( ojsumas["otros"] );
                        
                        
                    }
                    else{
                       
                        main_lista (obj);

                    }

                    

  
                         resolve( xhr );

                    loader.fin();
                }
            };
            xhr.onerror = function (e) {                    
                    reject(
                        xhr.status,
                        xhr.response   
                    );                 

            };                       

            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( null );                       


    })

    return promise;






};








