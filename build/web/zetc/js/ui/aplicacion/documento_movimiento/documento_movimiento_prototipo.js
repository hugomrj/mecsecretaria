

/* global loader, form, ajax, arasa, boton, reflex, tabla */

function DocumentoMovimiento(){

    this.tipo = "documento_movimiento";   
    this.recurso = "documentosmovimientos";   
    this.value = 0;


    this.dom="";
    this.carpeta=  "/aplicacion";   


    this.titulosin = "Documento";
    this.tituloplu = "Documentos";   


    this.campoid=  'id';
    this.tablacampos =  ['fecha', 'oficina_interna.descripcion', 'observacion', ];

    //this.etiquetas =  ['fecha_ingreso', 'expediente_numero', 'procedencia', 'referencia' ];

    this.tablaformat = ['D', 'U', 'U'  ];                                  

    this.tbody_id = "movimiento-tb";

    this.botones_lista = [ this.lista_new] ;
    this.botones_form = "movimiento-acciones";   

    this.parent = null;

    
    this.combobox = 
        {
            "vehiculo":{
                "value":"vehiculo",
                "inner":"descripcion"
            }                

        };      
            
    
}






DocumentoMovimiento.prototype.form_ini = function() {    

};


DocumentoMovimiento.prototype.form_acciones = function(obj) {    
    
};


DocumentoMovimiento.prototype.form_validar = function() {    
    
    return true;
};










DocumentoMovimiento.prototype.main_list = function(obj, page) {    
    

};





DocumentoMovimiento.prototype.form_id_modal = function(  obj, json ) {   
    
 
    ajax.url = html.url.absolute()+'/aplicacion/documento_movimiento/htmf/form.html'; 
    ajax.metodo = "GET";  
    modal.ancho = 800;
    var obj = modal.ventana.mostrar("dmslf");           
    
    
    // cargara datos
    form.name = "form_documento_movimiento";
    form.disabled(false);
    form.json = json;   
    form.llenar();       
    
    
    
    
    
    // llenar combo    
    var ojson = JSON.parse( form.json ) ;
    var valoption = (ojson['oficina_interna']['oficina_interna'] );
    var mov = new DocumentoMovimiento();
    mov.carga_combos(mov, valoption);    
    
    
    
    
    boton.objeto = "doc_movimiento"
    document.getElementById( 'documento_movimiento-acciones' ).innerHTML 
                        =  boton.basicform.get_botton_mrow();        
      
    
    
    var btn_doc_movimiento_salir = document.getElementById('btn_doc_movimiento_salir');              
    btn_doc_movimiento_salir.onclick = function(  )
    {  
        var ven = document.getElementById('vntdmslf');
        modal.ventana.cerrar(ven);     
    };       
    
    
    var btn_doc_movimiento_modificar = document.getElementById('btn_doc_movimiento_modificar');              
    btn_doc_movimiento_modificar.onclick = function(  )
    {  
            
        var obj_id = document.getElementById('documento_movimiento_id').value;
        
        var obj = new DocumentoMovimiento();
        obj.form_editar_modal(obj, obj_id);
        
     
    };       
        
    
    
    
    
    var btn_doc_movimiento_eliminar = document.getElementById('btn_doc_movimiento_eliminar');              
    btn_doc_movimiento_eliminar.onclick = function(){     

            loader.inicio();

            form.name = "form_"+obj.tipo;                        
            var id = document.getElementById( 'documento_movimiento_id').value;


            var xhr =  new XMLHttpRequest();     
            var metodo = "DELETE";        
            var url = arasa.html.url.absolute()  +"/api/documentosmovimientos/"+id;     


            ajax.json = null;
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    loader.fin();
                    ajax.local.token =  xhr.getResponseHeader("token") ;            
                    localStorage.setItem('token', xhr.getResponseHeader("token") );     


                    switch (xhr.status) {

                      case 200:

                            msg.ok.mostrar("registro eliminado");          
                            //reflex.data  = xhr.responseText;
                            btn_doc_movimiento_salir.onclick();
                            
                            var objDoc = new Documento();
                            objDoc.dom = 'arti_form';
                            reflex.form_id_promise( objDoc, 
                                document.getElementById('documento_documento').value
                                ) ;                                                                                        
                            break; 

                      default: 
                        msg.error.mostrar("error de acceso");           
                    }                        


                }
            };
            xhr.onerror = function (e) {                    
                reject( xhr );                 
            };  


            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( ajax.json );      

    }    
    
    

}



DocumentoMovimiento.prototype.sublista = function(json) {    
              
            
    var ojson = JSON.parse(json) ; 
    
    var det = ojson['moviemientos'];

    var sub = new DocumentoMovimiento();
    

    tabla.json = JSON.stringify(det);
    tabla.ini(sub);
    tabla.id = "movimiento-tabla";
    tabla.tbody_id = "movimiento-tb"    
    tabla.gene();  
    tabla.formato(sub);
  
  
    // acciones sobre registros

    //tabla.lista_registro(sub, this.sublista_form_id );     
   tabla.lista_registro(sub, this.form_id_modal_promise );     
    
  
  
    // accciones de     
    boton.blabels = ['Mover documento']
    document.getElementById( 'movimiento_acciones_lista' ).innerHTML 
                =  boton.get_botton_base();  
    
        


    var movimiento_agregar = document.getElementById('btn_documento_mover documento');              
    movimiento_agregar.onclick = function(  )
    {  
        
        ajax.url = html.url.absolute()+'/aplicacion/documento_movimiento/htmf/form.html'; 
        ajax.metodo = "GET";  
        modal.ancho = 800;
        var obj = modal.ventana.mostrar("ind");       
        
        var obj_det = new DocumentoMovimiento();            
        obj_det.new(obj_det);
        
    };       
  
    
    
}







DocumentoMovimiento.prototype.new = function( obj  ) {                
  
    var det = new DocumentoMovimiento();
    det.form_ini();
    det.form_acciones(det);
    det.carga_combos(det);
    
    
    
    // valor de cabecera 
    var documento_movimiento_documento = document.getElementById('documento_movimiento_documento');
    documento_movimiento_documento.value = document.getElementById('documento_documento').value;




    var fecha = new Date(); //Fecha actual
    var mes = fecha.getMonth()+1; //obteniendo mes
    var dia = fecha.getDate(); //obteniendo dia
    var ano = fecha.getFullYear(); //obteniendo año
    if(dia<10)
      dia='0'+dia; //agrega cero si el menor de 10
    if(mes<10)
      mes='0'+mes //agrega cero si el menor de 10
    var f =   ano+"-"+mes+"-"+dia;      
    
    var documento_movimiento_fecha = document.getElementById('documento_movimiento_fecha');
    documento_movimiento_fecha.value = f;   
      
  
  /*
    var detalle_cantidad_hoja = document.getElementById('detalle_cantidad_hoja');             
    detalle_cantidad_hoja.focus();
    detalle_cantidad_hoja.select();    
*/
    
    
    boton.ini(obj);
    document.getElementById( 'documento_movimiento-acciones' ).innerHTML 
                        =  boton.basicform.get_botton_okcan();        
  
  
  
  
    var btn_otd_cancelar = document.getElementById('btn_documento_movimiento_cancelar');
    btn_otd_cancelar.addEventListener('click',
        function(event) {    

            var ven = document.getElementById('vntind');
            modal.ventana.cerrar(ven);     
            
        },
        false
    ); 
  
  
  
  
    var btn_otd_aceptar = document.getElementById('btn_documento_movimiento_aceptar');
    btn_otd_aceptar.addEventListener('click',
        function(event) {               



            if ( obj.form_validar()) {

                loader.inicio();        

                form.name = "form_"+obj.tipo;

                var xhr =  new XMLHttpRequest();     
                var url = arasa.html.url.absolute()  +"/api/"+ obj.recurso;   

                var metodo = "POST";                                
                        
                ajax.json = form.datos.getjson() ;                            


                xhr.open( metodo.toUpperCase(),   url,  true );     

                xhr.onreadystatechange = function () {
                    if (this.readyState == 4 ){

                        //ajax.headers.get();
                        ajax.local.token =  xhr.getResponseHeader("token") ;            
                        localStorage.setItem('token', xhr.getResponseHeader("token") );     
                        //ajax.state = xhr.status;


                        switch (xhr.status) {

                          case 200:

                                msg.ok.mostrar("registro agregado");          
                                reflex.data  = xhr.responseText;
             
                                
                                var cab = new Documento();    
                                cab.dom = 'arti_form';     
                                var val_cab = document.getElementById('documento_movimiento_documento');
                               
                                
                                reflex.form_id_promise( cab, val_cab.value ) ;                                                                                         
                                 
                                btn_otd_cancelar.click();                                

                                break; 


                          case 401:

                                window.location =  html.url.absolute();         
                                break;                             



                          default: 
                            msg.error.mostrar("error de acceso");           
                        }                        

                        //arasa.html.url.redirect( xhr.status );                            
                        loader.fin();


                    }
                };
                xhr.onerror = function (e) {                    
                    reject( xhr );                 
                };  


                var type = "application/json";
                xhr.setRequestHeader('Content-Type', type);   
                //ajax.headers.set();
                xhr.setRequestHeader("token", localStorage.getItem('token'));           

                xhr.send( ajax.json );      

            }



        },
        false
    )




   
 
  
};






DocumentoMovimiento.prototype.carga_combos = function( obj , sel ) {
    
    var ofi = new OficinaInterna(); 
    ofi.combobox("oficina_interna", sel);        
    
    
};







DocumentoMovimiento.prototype.form_id_modal_promise = function ( obj, id ) {    



    const promise = new Promise((resolve, reject) => {


        loader.inicio();

        var comp = window.location.pathname;                 
        var path =  comp.replace(arasa.html.url.absolute() , "");

        var xhr =  new XMLHttpRequest();            
        var url = arasa.html.url.absolute() +"/api/"+ obj.recurso + '/'+id;   


        var metodo = "GET";                         
        ajax.json  = null;
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                ajax.local.token =  xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token") );     

                
                obj.form_id_modal(obj, xhr.responseText)
                
                
                resolve( xhr );
                loader.fin();


            }
        };
        xhr.onerror = function (e) {                    
            reject(
                  xhr.status,
                  xhr.response   
            );                 

        };                       

        xhr.setRequestHeader("path", path );
        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);   
        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( ajax.json  );                       

    })

    return promise;



    };



DocumentoMovimiento.prototype.form_editar_modal = function( obj, id ) {
    
    
    form.name = "form_documento_movimiento";
    form.disabled(true);
    
    
    
    boton.objeto = "doc_movimiento"
    document.getElementById( 'documento_movimiento-acciones' ).innerHTML 
            =  boton.basicform.get_botton_edit();        
                
                
                
   


    var btn_doc_movimiento_cancelar = document.getElementById('btn_doc_movimiento_cancelar');    
    btn_doc_movimiento_cancelar.onclick = function() {   
    
        var ven = document.getElementById('vntdmslf');
        modal.ventana.cerrar(ven);             
        obj.form_id_modal_promise(obj, id);
    
    }      
    

    
    
    var btn_doc_movimiento_editar = document.getElementById('btn_doc_movimiento_editar');    
    btn_doc_movimiento_editar.onclick = function() {   
    
        
                    if ( obj.form_validar()) {
                                       
                        loader.inicio();

                        form.name = "form_"+obj.tipo;                        
                        var id = document.getElementById( obj.tipo + '_'  + obj.campoid ).value;

                        var xhr =  new XMLHttpRequest();     
                        var metodo = "PUT";        
                        var url = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+id;     
                        
                        ajax.json = form.datos.getjson() ;                            
                        
                        

                        xhr.open( metodo.toUpperCase(),   url,  true );      


                        xhr.onreadystatechange = function () {
                            if (this.readyState == 4 ){

                                //ajax.headers.get();
                                ajax.local.token =  xhr.getResponseHeader("token") ;            
                                localStorage.setItem('token', xhr.getResponseHeader("token") );     
                                //ajax.state = xhr.status;

                                switch (xhr.status) {

                                  case 200:

                                        msg.ok.mostrar("registro agregado");          
                                        reflex.data  = xhr.responseText;


                                        var cab = new Documento();    
                                        cab.dom = 'arti_form';     
                                        var val_cab = document.getElementById('documento_movimiento_documento');


                                        btn_doc_movimiento_cancelar.click();    
                                        
                                        break;

                                  case 401:

                                        window.location = html.url.absolute()  ;         
                                        break;                             


                                  case 500:

                                        msg.error.mostrar(  reflex.data  );          
                                        break; 


                                  case 502:                              
                                        msg.error.mostrar(  reflex.data  );          
                                        break;                                 


                                  default: 
                                    msg.error.mostrar("error de acceso");           
                                }                        

                                //arasa.html.url.redirect( xhr.status );                            
                                loader.fin();


                            }
                        };
                        xhr.onerror = function (e) {                    
                            reject( xhr );                 
                        };  


                        var type = "application/json";
                        xhr.setRequestHeader('Content-Type', type);   
                        //ajax.headers.set();
                        xhr.setRequestHeader("token", localStorage.getItem('token'));           

                        xhr.send( ajax.json );      

                        
                    }        
        



    }      
    
    
         
      
    
    
};
