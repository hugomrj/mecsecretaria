

function main_form (obj) {  

    fetch( html.url.absolute() + '/consulta/vehir/htmf/cab.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
          
            document.getElementById('arti_form').innerHTML = data ;
          
            // cargar combo programas
            var vehi = new Vehiculo(); 
            vehi.combobox("vehiculo");    
            
            var domobj = document.getElementById("vehiculo");
            var opt = document.createElement('option');            
            opt.value = "-1";
            opt.innerHTML = "Todos los vehiculos";                        
            domobj.appendChild(opt);                
                    
            main_lista(obj);
            
      })

};







function main_lista (obj) {    

    fetch( html.url.absolute() + '/consulta/vehir/htmf/det.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('main_lista').innerHTML = data ;        
        form_ini (obj);            
        
      })

};






function form_ini (obj) {    

    // boton enviar
    var btn_cab_buscar = document.getElementById('btn_cab_buscar');
    btn_cab_buscar.onclick = function(event) {     

        var fecha_desde = document.getElementById('fecha_desde');
        var fecha_hasta = document.getElementById('fecha_hasta');

        if (fecha_desde.value != "" &&  fecha_hasta.value != "" ){        
            Consulta_detalle_promesa( obj  );
        }
        
    }
    

    
    
    
    

    var btn_xlsx = document.getElementById( 'btn_xlsx');
    btn_xlsx.onclick = function()
    {  

        var fecha_desde = document.getElementById('fecha_desde');
        var fecha_hasta = document.getElementById('fecha_hasta');

        if (fecha_desde.value != "" &&  fecha_hasta.value != "" ){        

            loader.inicio();

            var url = html.url.absolute() + '/api/utilizacionvehiculo_qry/consulta2/xlsx/'                
                +';fe1='+document.getElementById("fecha_desde").value 
                +';fe2='+document.getElementById("fecha_hasta").value 
                +';veh='+document.getElementById("vehiculo").value 


            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {

                if (xhttp.readyState === 4 && xhttp.status === 200) {

                    var file_name = "vehiculos_resumen.xlsx" ; 

                    // Trick for making downloadable link
                    var a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    a.download = file_name;
                    a.click();
                    loader.fin();

                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.setRequestHeader("token", localStorage.getItem('token'));           
            xhttp.send();

        }        
    }
          
    
          
};







function Consulta_detalle_promesa( obj ) {    


    const promise = new Promise((resolve, reject) => {

            loader.inicio();            

            var url = html.url.absolute() + '/api/utilizacionvehiculo_qry/consulta2/'                
                +';fe1='+document.getElementById("fecha_desde").value 
                +';fe2='+document.getElementById("fecha_hasta").value 
                +';veh='+document.getElementById("vehiculo").value 
                

            var xhr =  new XMLHttpRequest();      

            var metodo = "GET";                                     
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    tabla.json = xhr.responseText;

                    var ojson = JSON.parse( tabla.json ) ; 
                    tabla.json = JSON.stringify(ojson['datos']) ;  
                    
                                       
                   
                    if (tabla.json != "[]"){
                    
                        tabla.ini(obj);                            
                        tabla.gene();                                  
                        tabla.formato(obj);                            
                                                
                        
                        // suma de consultas
                        var jsumas = JSON.stringify(ojson['summary'][0]) ;              
                        var ojsumas = JSON.parse(jsumas) ;

                        
                        document.getElementById("cantidad").innerHTML
                            =  fmtNum( ojsumas["cantidad"] );
                        
                        document.getElementById("zafira").innerHTML
                            =  fmtNum( ojsumas["zafira"] );
                        
                        document.getElementById("zafira").innerHTML
                            =  fmtNum( ojsumas["zafira"] );
                        
                        document.getElementById("temporal").innerHTML
                            =  fmtNum( ojsumas["temporal"] );
                        
                        document.getElementById("tacumbu").innerHTML
                            =  fmtNum( ojsumas["tacumbu"] );
                        
                        document.getElementById("camsap").innerHTML
                            =  fmtNum( ojsumas["camsap"] );
                        
                        document.getElementById("camsap").innerHTML
                            =  fmtNum( ojsumas["camsap"] );
                        
                        document.getElementById("otro").innerHTML
                            =  fmtNum( ojsumas["otro"] );
                        
                        document.getElementById("kms").innerHTML
                            =  fmtNum( ojsumas["kms"] );
                        
                        
                        
/*
                        

                        document.getElementById("sexo_masculino").innerHTML
                            =  fmtNum( ojsumas["sexo_masculino"] );

                        document.getElementById("sexo_femenino").innerHTML
                            =  fmtNum( ojsumas["sexo_femenino"] );

                        document.getElementById("sexo_otros").innerHTML
                            =  fmtNum( ojsumas["sexo_otros"] );


                        document.getElementById("zona1").innerHTML
                            =  fmtNum( ojsumas["zona1"] );

                        document.getElementById("zona2").innerHTML
                            =  fmtNum( ojsumas["zona2"] );

                        document.getElementById("zona3").innerHTML
                            =  fmtNum( ojsumas["zona3"] );

                        document.getElementById("zona4").innerHTML
                            =  fmtNum( ojsumas["zona4"] );

                        document.getElementById("zona5").innerHTML
                            =  fmtNum( ojsumas["zona5"] );

                        document.getElementById("zona6").innerHTML
                            =  fmtNum( ojsumas["zona6"] );

                        document.getElementById("zona7").innerHTML
                            =  fmtNum( ojsumas["zona7"] );

                        document.getElementById("zona8").innerHTML
                            =  fmtNum( ojsumas["zona8"] );

                        document.getElementById("zona9").innerHTML
                            =  fmtNum( ojsumas["zona9"] );

                        document.getElementById("zona_banco_hovy").innerHTML
                            =  fmtNum( ojsumas["zona_banco_hovy"] );

                        document.getElementById("zona_refugio_sen").innerHTML
                            =  fmtNum( ojsumas["zona_refugio_sen"] );

                        document.getElementById("zona_otro").innerHTML
                            =  fmtNum( ojsumas["zona_otro"] );


                        document.getElementById("edad_5_12").innerHTML
                            =  fmtNum( ojsumas["edad_5_12"] );

                        document.getElementById("edad_13_17").innerHTML
                            =  fmtNum( ojsumas["edad_13_17"] );

                        document.getElementById("edad_18_29").innerHTML
                            =  fmtNum( ojsumas["edad_18_29"] );

                        document.getElementById("edad_30_49").innerHTML
                            =  fmtNum( ojsumas["edad_30_49"] );

                        document.getElementById("edad_50_64").innerHTML
                            =  fmtNum( ojsumas["edad_50_64"] );

                        document.getElementById("edad_65_mas").innerHTML
                            =  fmtNum( ojsumas["edad_65_mas"] );
*/


                    }
                    else{
                       
                        main_lista (obj);
                        
                    }
  
                         resolve( xhr );

                    loader.fin();
                }
            };
            xhr.onerror = function (e) {                    
                    reject(
                        xhr.status,
                        xhr.response   
                    );                 

            };                       

            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( null );                       

    })

    return promise;


};








