

/* global boton */

function DocumentoSalida(){
    
   this.tipo = "documento_salida";   
   this.recurso = "documentossalidas";   
   this.value = 0;
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Documento salida";
   this.tituloplu = "Documentos salidas";   
      
   
   this.campoid=  'documento';
   this.tablacampos =  ['fecha_ingreso', 'expediente_numero',  'fecha_salida',
                'memo_numero', 'procedencia', 'destino',
                'archivadoen'];
   
   this.etiquetas =  ['fecha_ingreso', 'expediente_numero',  'fecha_salida',
                'memo_numero', 'procedencia', 'destino',
                'archivadoen'];
   
   this.tablaformat = ['D', 'C', 'D',
                       'T', 'C', 'U',
                       'U'
                   ];                                  
   
   this.tbody_id = "documento_salida-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "documento_salida-acciones";   
         
   this.parent = null;
   
    this.filtro = "";
   
   }





DocumentoSalida.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);  
    
    
        
    
    
    var fecha = new Date(); //Fecha actual
    var mes = fecha.getMonth()+1; //obteniendo mes
    var dia = fecha.getDate(); //obteniendo dia
    var ano = fecha.getFullYear(); //obteniendo año
    if(dia<10)
      dia='0'+dia; //agrega cero si el menor de 10
    if(mes<10)
      mes='0'+mes //agrega cero si el menor de 10
    var f =   ano+"-"+mes+"-"+dia;      
    
    var documento_fecha_ingreso = document.getElementById('documento_fecha_ingreso');
    documento_fecha_ingreso.value = f;
    
};





DocumentoSalida.prototype.form_ini = function() {    


};






DocumentoSalida.prototype.form_validar = function() {    

    return true;
};










DocumentoSalida.prototype.main_list = function(obj, page) {    

    

    if (page === undefined) {    
        page = 1;
    }
    
    //let promesa = arasa.vista.lista_paginacion(obj, page);
    let promesa = this.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML  
                =  "";                
            /*
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    obj.new( obj );
                },
                false
            );              
            */
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 



};






DocumentoSalida.prototype.tabuladores = function(obj) {    

    
}




DocumentoSalida.prototype.post_form_id = function(obj, id) {    
    

}



DocumentoSalida.prototype.lista_paginacion = function(obj, page) {    
    
    const promise = new Promise((resolve, reject) => {

        loader.inicio();
        obj.page = page;

        var comp = window.location.pathname;                 
        var path =  comp.replace( arasa.html.url.absolute() , "");

        var xhr =  new XMLHttpRequest();           


        var filtro = ""        
        if(typeof obj.getUrlFiltro === 'function') {
          filtro = obj.getUrlFiltro(obj);
        }                    



        var url = arasa.html.url.absolute() +"/api/"+obj.recurso; 
        url = url + filtro;
        url = url + "?page=" + obj.page;


        var metodo = "GET";                         
        ajax.json  = null;
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                arasa.vista.tabla_html(obj)
                    .then(( text ) => {

                        // botones de accion - nuevo para este caso

                        ajax.local.token =  xhr.getResponseHeader("token") ;            
                        localStorage.setItem('token', xhr.getResponseHeader("token") );     


                        var ojson = JSON.parse( xhr.responseText ) ;     
                        tabla.json = JSON.stringify(ojson['datos']) ;


                        tabla.ini(obj);
                        tabla.gene();   
                        tabla.formato(obj);

                        tabla.set.tablaid(obj);     
                        //tabla.lista_registro(obj, this.form_id_promise ); 
                        tabla.lista_registro(obj, obj.form_id_promise ); 

                        var json_paginacion = JSON.stringify(ojson['paginacion']);

                        arasa.vista.paginacion_html(obj, json_paginacion );

                        ajax.state = xhr.status;

                            resolve( xhr );

                        loader.fin();

                    })

                //arasa.vista.tabla_html(obj);
            }
        };
        xhr.onerror = function (e) {                    
            reject(
                  xhr.status,
                  xhr.response   
            );                 

        };                       

        xhr.setRequestHeader("path", path );
        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);   

        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( ajax.json  );                       


    })

    return promise;



};





DocumentoSalida.prototype.form_id_promise = function ( obj, id ) {    

//alert("DocumentoSalida.prototype.form_id_promise = function ")

    const promise = new Promise((resolve, reject) => {

        loader.inicio();

        var comp = window.location.pathname;                 
        var path =  comp.replace(arasa.html.url.absolute() , "");

        var xhr =  new XMLHttpRequest();            
        var url = arasa.html.url.absolute() +"/api/"+ obj.recurso + '/'+id;   


        var metodo = "GET";                         
        ajax.json  = null;
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                ajax.local.token =  xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token") );     
                ajax.state = xhr.status;

                
                
                ajax.url =  html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/form.html';                    
                ajax.metodo = "GET";        
                document.getElementById(  obj.dom ).innerHTML =  ajax.public.html();                   
                

                // formulario cabecera
                
                form.name = "form_documento";
                form.disabled(false);
                form.json = xhr.responseText;   
                form.llenar();                
                
                
                
                // condicion  si existe o no campo salida
                
                var ojson = JSON.parse(xhr.responseText) ;                 
                var salida  = ojson['salida']; 
                
     

                 // nueva salida
                 if (typeof(salida) === "undefined") {
                     
                 
                    
                    var fecha = new Date(); //Fecha actual
                    var mes = fecha.getMonth()+1; //obteniendo mes
                    var dia = fecha.getDate(); //obteniendo dia
                    var ano = fecha.getFullYear(); //obteniendo año
                    if(dia<10)
                      dia='0'+dia; //agrega cero si el menor de 10
                    if(mes<10)
                      mes='0'+mes //agrega cero si el menor de 10
                    var f =   ano+"-"+mes+"-"+dia;      

                    var documento_salida_fecha_salida = document.getElementById('documento_salida_fecha_salida');
                    documento_salida_fecha_salida.value = f;

                    
                    var documento_salida_memo_numero = document.getElementById('documento_salida_memo_numero');
                    documento_salida_memo_numero.value = 0;
                    documento_salida_memo_numero.disabled = true;                    
                    
                    
                    
                    
                    
                    boton.objeto = "salida";  
                    boton.blabels = ['Dar Salida', 'Atras'];
                    document.getElementById( 'documento_salida-acciones' ).innerHTML 
                        =  boton.get_botton_base();  
                
                
                
                    var btn_salida_atras = document.getElementById('btn_salida_atras');
                    btn_salida_atras.addEventListener('click',
                        function(event) {    
                            obj.main_list(obj, 1); 
                        },
                        false
                    ); 
                
                
                    var btn_salida_dar = document.getElementById('btn_salida_dar salida');
                    btn_salida_dar.addEventListener('click',
                        function(event) {    
                            
                            var documento_salida_documento = document.getElementById('documento_salida_documento');
                            documento_salida_documento.value = document.getElementById('documento_documento').value;
                            

                            


                            var documentosalida = new DocumentoSalida();    


                            if ( documentosalida.form_validar()) {

                                loader.inicio();        

                                form.name = "form_documento_salida";

                                var xhr =  new XMLHttpRequest();     
                                var url = arasa.html.url.absolute()  +"/api/"+ documentosalida.recurso;   

                                var metodo = "POST";                                
                                ajax.json = form.datos.getjson() ;                            

                                xhr.open( metodo.toUpperCase(),   url,  true );     

                                xhr.onreadystatechange = function () {
                                    if (this.readyState == 4 ){

                                        //ajax.headers.get();
                                        ajax.local.token =  xhr.getResponseHeader("token") ;            
                                        localStorage.setItem('token', xhr.getResponseHeader("token") );     
                       
                       
                                        switch (xhr.status) {

                                          case 200:

                                                msg.ok.mostrar("registro agregado");          
                                                reflex.data  = xhr.responseText;
                                                
                                                obj.form_id_promise(obj, documento_salida_documento.value );
                                                
                                                break; 


                                          case 401:

                                                window.location =  html.url.absolute();         
                                                break;                             


                                          default: 
                                            msg.error.mostrar("error de acceso");           
                                        }                        
                                        loader.fin();


                                    }
                                };
                                xhr.onerror = function (e) {                    
                                    reject( xhr );                 
                                };  


                                var type = "application/json";
                                xhr.setRequestHeader('Content-Type', type);   
                                //ajax.headers.set();
                                xhr.setRequestHeader("token", localStorage.getItem('token'));           

                                xhr.send( ajax.json );      

                            }      
                        },
                        false
                    ); 
                     
                     
                 }
                 // si existe salida
                 else
                 {
                     
                     
                    form.name = "form_documento_salida";
                    form.disabled(false);                     
                    form.json = JSON.stringify(salida);   
                    form.llenar();                          
                     
                     
                    boton.objeto = "salida";                      
                    document.getElementById( 'documento_salida-acciones' ).innerHTML 
                        =  boton.basicform.get_botton_mrow();  
                                 
                    
                    
                    // nivel 1
                    var btn_salida_salir = document.getElementById('btn_salida_salir');
                    btn_salida_salir.addEventListener('click',
                        function(event) {    
                            obj.main_list(obj, 1); 
                        },
                        false
                    ); 
                                     
                     
                    // nivel 1
                    var btn_salida_eliminar = document.getElementById('btn_salida_eliminar');
                    btn_salida_eliminar.onclick = function(){ 
                        
   
                            boton.objeto = "salida";  
                            boton.blabels = ['Eliminar', 'Cancelar'];
                            document.getElementById( 'documento_salida-acciones' ).innerHTML 
                                =  boton.get_botton_base();                              
                            
                            
                            var btn_salida_cancelar = document.getElementById('btn_salida_cancelar');
                            btn_salida_cancelar.addEventListener('click',
                                function(event) {                                        
                                    obj.form_id_promise(obj, documento_salida_documento.value );
                                },
                                false
                            ); 

                            // 2
                            var btn_salida_eliminar2 = document.getElementById('btn_salida_eliminar');
                            btn_salida_eliminar2.onclick = function(){     
                            
            
                                    loader.inicio();

                                    form.name = "form_"+obj.tipo;                        
                                    var id = document.getElementById( 'documento_salida_id').value;

                                    var xhr =  new XMLHttpRequest();     
                                    var metodo = "DELETE";        
                                    var url = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+id;     

                                    
                                    ajax.json = null;
                                    xhr.open( metodo.toUpperCase(),   url,  true );      

                                    xhr.onreadystatechange = function () {
                                        if (this.readyState == 4 ){

                                            loader.fin();
                                            ajax.local.token =  xhr.getResponseHeader("token") ;            
                                            localStorage.setItem('token', xhr.getResponseHeader("token") );     
                                            

                                            switch (xhr.status) {

                                              case 200:

                                                    msg.ok.mostrar("registro eliminado");          
                                                    //reflex.data  = xhr.responseText;
                                                    obj.form_id_promise(obj, documento_salida_documento.value );
                                                    break; 
/*
                                              case 401:
                                                    window.location = html.url.absolute() ;         
                                                    break;                             
*/

                                              default: 
                                                msg.error.mostrar("error de acceso");           
                                            }                        

                                            
                                        }
                                    };
                                    xhr.onerror = function (e) {                    
                                        reject( xhr );                 
                                    };  


                                    var type = "application/json";
                                    xhr.setRequestHeader('Content-Type', type);   
                                    xhr.setRequestHeader("token", localStorage.getItem('token'));           
                                    xhr.send( ajax.json );      

                            }
                            
                        }
                        
                        
                        
                    // nivel 1
                    var btn_salida_modificar = document.getElementById('btn_salida_modificar');
                    btn_salida_modificar.addEventListener('click',
                        function(event) {    
                            
                            form.name = "form_documento_salida";  
                            form.disabled(true);
                            
                            
                            
                            boton.objeto = "salida";  
                            boton.blabels = ['Modificar', 'Cancelar'];
                            document.getElementById( 'documento_salida-acciones' ).innerHTML 
                                =  boton.get_botton_base();                                 
                            
                            
                            
                            var btn_salida_cancelar = document.getElementById('btn_salida_cancelar');
                            btn_salida_cancelar.onclick = function(){                                 
                                obj.form_id_promise(obj, documento_salida_documento.value );                            
                            }

                            var btn_salida_modificar = document.getElementById('btn_salida_modificar');
                            btn_salida_modificar.onclick = function(){                                 
                                
                                
                                if ( obj.form_validar()) {

                                    loader.inicio();

                                    form.name = "form_documento_salida";                        
                                    var id = document.getElementById( 'documento_salida_id').value;

                                    var xhr =  new XMLHttpRequest();     
                                    var metodo = "PUT";        
                                    var url = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+id;     

                                    ajax.json = form.datos.getjson() ;                            
console.log(ajax.json)                                    

                                    xhr.open( metodo.toUpperCase(),   url,  true );      


                                    xhr.onreadystatechange = function () {
                                        if (this.readyState == 4 ){

                                            //ajax.headers.get();
                                            ajax.local.token =  xhr.getResponseHeader("token") ;            
                                            localStorage.setItem('token', xhr.getResponseHeader("token") );     
                                            //ajax.state = xhr.status;

                                            switch (xhr.status) {

                                              case 200:


                                                    reflex.data  = xhr.responseText;
                                                    obj.form_id_promise(obj, documento_salida_documento.value );  
                                                    
                                                    msg.ok.mostrar("registro editado");
                                                    break; 


                                              case 401:
                                                    window.location = html.url.absolute()  ;         
                                                    break;                             


                                              case 500:
                                                    msg.error.mostrar(  reflex.data  );          
                                                    break; 


                                              case 502:                              
                                                    msg.error.mostrar(  reflex.data  );          
                                                    break;                                 


                                              default: 
                                                msg.error.mostrar("error de acceso");           
                                            }                        
                         
                                            loader.fin();


                                        }
                                    };
                                    xhr.onerror = function (e) {                    
                                        reject( xhr );                 
                                    };  


                                    var type = "application/json";
                                    xhr.setRequestHeader('Content-Type', type);   
                                    //ajax.headers.set();
                                    xhr.setRequestHeader("token", localStorage.getItem('token'));   
                                    xhr.send( ajax.json );      


                                }                                
                                
                                
                            }                            
                            
                            
                        },
                        false
                    );                         
                        
                        
                        
                    
                 }
                
                
                
                
                resolve( xhr );
                loader.fin();


            }
        };
        xhr.onerror = function (e) {                    
            reject(
                  xhr.status,
                  xhr.response   
            );                 

        };                       

        xhr.setRequestHeader("path", path );
        var type = "application/json";
        xhr.setRequestHeader('Content-Type', type);   
        xhr.setRequestHeader("token", localStorage.getItem('token'));           

        xhr.send( ajax.json  );                       

    })

    return promise;



    };




DocumentoSalida.prototype.getUrlFiltro = function( obj  ) {                
    
    var ret = "";    
      
    ret = obj.filtro;
    return ret;
};










DocumentoSalida.prototype.icobusqueda = function( obj  ) {                
    
//fetch(  html.url.absolute() + '/com/busqueda/busqueda_bar.html' )

    //fetch( html.url.absolute() + '/aplicacion/documento_salida/htmf/qry/busqueda_bar.html' )
    fetch(  html.url.absolute() + '/com/busqueda/busqueda_bar.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        document.getElementById( "bar_busqueda" ).innerHTML =  data;                    

        //html.topbar.busqueda_bar_accion(obj);
            
            var search_bar = document.getElementById( "icon-search_bar" );
            search_bar.addEventListener('click',
                function(event) {      


                    var b1 = document.getElementById( "icon-search_bar" ); 
                    b1.style.visibility = "hidden";        
                    b1.parentNode.style.display = "none";     

                    var b2 = document.getElementById( "icon-up_bar" ); 
                    b2.style.visibility = "visible";        
                    b2.parentNode.style.display = "block";     



                    
                    fetch( html.url.absolute() + '/aplicacion/documento/htmf/qry/busqueda.html' )
                      .then(response => {
                        return response.text();
                      })
                      .then(data => {
                        document.getElementById( "divbusqueda" ).innerHTML =  data;            

                        // cargar combo año
                        var domobj = document.getElementById("qry_agno");
                        
                        var today = new Date();
                        var year = today.getFullYear();
                        var firstyear = 2000;
                        var rango = year - firstyear ;


                        for( x=0; x < rango ; x++ ) {
                            var opt = document.createElement('option');                         
                            opt.value = year;      
                            opt.innerHTML = year;                                        
                            domobj.appendChild(opt);                     
                            year--;
                        }                           

                      
                      
                        var btn_busqueda = document.getElementById( "btn_busqueda" );                        
                        btn_busqueda.onclick = function(){                                  
                            
                            var tex = document.getElementById("qry_busquedatexto").value;
                            var aa = document.getElementById("qry_agno").value ;

                            obj.filtro = ';q=' + tex + ';aa=' +aa  ;

                            obj.main_list(obj, 1); 
                         
                        }
                        
       

                      
                      
                      
                      
                      })            


                },
                false
            );     




            var up_bar = document.getElementById( "icon-up_bar" );
            up_bar.addEventListener('click',
                function(event) {      

                    var b1 = document.getElementById( "icon-search_bar" ); 
                    b1.style.visibility = "visible";        
                    b1.parentNode.style.display = "block";  

                    var b2 = document.getElementById( "icon-up_bar" ); 
                    b2.style.visibility = "hidden";        
                    b2.parentNode.style.display = "none";      

                    document.getElementById( "divbusqueda" ).innerHTML =  "";            

                    obj.filtro = "";    
                    obj.main_list(obj, 1); 

                },
                false
            );     
        
        

 
        
        
        
      })   
    
    
};



