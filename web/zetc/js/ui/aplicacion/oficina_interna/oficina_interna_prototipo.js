

function OficinaInterna(){
    
   this.tipo = "oficinainterna";   
   this.recurso = "oficinasinternas";   
   this.value = 0;
   
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
   
   this.campoid=  'vehiculo';
   this.tablacampos =  ['codigo', 'marca' , 'modelo', 'chapa' ];
         
   
}








OficinaInterna.prototype.combobox = function( dom, sel ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    

    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));



    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {

            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            
      
            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['oficina_interna'] );            

                if (idedovalue != jsonvalue )
                {   
                    var opt = document.createElement('option');     
                    
                    opt.value = jsonvalue;
      
                    opt.innerHTML = oJson[x]['descripcion'] ;                        
                
                    domobj.appendChild(opt);                     
                }
            }                        
                //resolve( data );
            loader.fin();
        })
        .then(status => {
            
            if (!(sel === undefined)) {                    
                var domobj = document.getElementById(dom);                    
                var cnodes = domobj.childnodes;

                for (var i = 0; i < domobj.length; i++) 
                {
                    var opt = domobj[i];
                    if (opt.value == sel){     
                        domobj[i].selected = true;
                        return;
                    }                                  
                }                
            }   
            
        })
        /*
        .catch(function(error) {
            console.log(error);            
        });
        */

}





