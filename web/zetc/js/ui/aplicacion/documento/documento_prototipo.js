

function Documento(){
    
   this.tipo = "documento";   
   this.recurso = "documentos";   
   this.value = 0;
   this.form_descrip = "documento_descripcion";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Documento";
   this.tituloplu = "Documentos";   
      
   
   this.campoid=  'documento';
   this.tablacampos =  ['fecha_ingreso', 'expediente_numero', 'procedencia', 'referencia' ];
   
   this.etiquetas =  ['fecha_ingreso', 'expediente_numero', 'procedencia', 'referencia' ];
    
   this.tablaformat = ['D', 'C', 'C', 'C' ];                                  
   
   this.tbody_id = "documento-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "documento-acciones";   
         
   this.parent = null;   
   this.tabs =  ['Movimientos'];
   
   this.filtro = "";
}





Documento.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);  
    
    
        
    
    
    var fecha = new Date(); //Fecha actual
    var mes = fecha.getMonth()+1; //obteniendo mes
    var dia = fecha.getDate(); //obteniendo dia
    var ano = fecha.getFullYear(); //obteniendo año
    if(dia<10)
      dia='0'+dia; //agrega cero si el menor de 10
    if(mes<10)
      mes='0'+mes //agrega cero si el menor de 10
    var f =   ano+"-"+mes+"-"+dia;      
    
    var documento_fecha_ingreso = document.getElementById('documento_fecha_ingreso');
    documento_fecha_ingreso.value = f;
    
    
    
    // no mostrar form de archivo
    var form_archivo = document.getElementById('form_archivo');      
    form_archivo.style.display = "none";        
          
    
    
};





Documento.prototype.form_ini = function() {    


};






Documento.prototype.form_validar = function() {    
    
    
    var documento_fecha_ingreso = document.getElementById('documento_fecha_ingreso');
    if (documento_fecha_ingreso.value == ""){
        msg.error.mostrar("Error en fecha ");                    
        documento_fecha_ingreso.focus();
        documento_fecha_ingreso.select();                                       
        return false;        
    }
    
    
    return true;
};










Documento.prototype.main_list = function(obj, page) {    

    

    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    obj.new( obj );
                },
                false
            );              
        
        })
       /*
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 
*/


};






Documento.prototype.tabuladores = function(obj) {    

    

    fetch( html.url.absolute() + '/aplicacion/documento_movimiento/htmf/sublista.html' )
      .then(response => {
        return response.text();
    })
    .then(data => {
                
        document.getElementById( "documento-tabuladores" ).innerHTML =  data;   
        
    
        var movimientos = new DocumentoMovimiento();
        movimientos.sublista(form.json);
    
    
            
    })
    
    
    
 
    // mostrar cuadricula de movimientos 
    
    
}




Documento.prototype.post_form_id = function(obj, id) {    
    
    
  /*
    var div_file_enviar = document.getElementById('div_file_enviar');
    div_file_enviar.style.display="none";
    
    var btn_file_cambiar = document.getElementById('btn_file_cambiar');
    btn_file_cambiar.style.display="none";
    */

    var div_file_borrar = document.getElementById('div_file_borrar');
    div_file_borrar.style.display="none";


    var fileD_nombre = document.getElementById('fileD_nombre');
    
    console.log( form.json   )



    var ojson = JSON.parse( form.json ) ;   
    var json = JSON.stringify(ojson['documento_digitalizadion']) ;       
    
    if (typeof json === 'undefined') 
    { 
        document.getElementById( 'fileD' ).style.display = "none";   
        document.getElementById( 'fileD' ).style.display = "none";   
        
                //result.innerHTML = "Variable is Undefined"; 
                
        document.getElementById( 'div_file_cambiar' ).style.display = "none";   
        document.getElementById( 'div_file_enviar' ).style.display = "flex";                   
                
    } 
    else 
    {   
        document.getElementById( 'div_file_cambiar' ).style.display = "flex";   
        document.getElementById( 'div_file_enviar' ).style.display = "none";   
        
        
        document.getElementById( 'fileU' ).style.display = "none";   

        var ojson = JSON.parse( json ) ;                                            
        document.getElementById( 'fileD_nombre' ).innerHTML  
                = ojson['file_name']





        var afileD = document.getElementById( 'afileD');
        afileD.onclick = function()
        {  
            var url = html.url.absolute() + "/Documento/Download";
            
            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                var a;
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    // Trick for making downloadable link
                    a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    // Give filename you wish to download

                    var file_name =  xhttp.getResponseHeader("file_name") ; 
                    
                    a.download = file_name;
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.setRequestHeader("documento", document.getElementById( 'documento_documento' ).value );

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.send();

        }


    }

    var btn_file_cambiar = document.getElementById( 'btn_file_cambiar');
    btn_file_cambiar.onclick = function()
    {  
        document.getElementById( 'fileD' ).style.display = "none";   
        document.getElementById( 'fileU' ).style.display = "initial";  

        document.getElementById( 'div_file_cambiar' ).style.display = "none";   
        document.getElementById( 'div_file_enviar' ).style.display = "flex";   
    };   
        
        
    // boton enviar
    var btn_file_enviar = document.getElementById('btn_file_enviar');
    btn_file_enviar.onclick = function(event) {     
       
       // var documento  = document.getElementById('documento_documento').value;              
        
        var filetxt = document.getElementById("migracion_file").files[0];
        var nombre_archivo = filetxt.name;

        var form = document.getElementById('form_archivo');
        var formdata = new FormData(form);

        formdata.append("filetxt", filetxt);
        
        
        file_upload_promesa( formdata, nombre_archivo, id )
            .then(( xhr ) => {                
                if (xhr.status == 200){                    
                     
                    reflex.form_id_promise( obj, id );                       
                    msg.ok.mostrar("archivo guardado");  
                }
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             

    }
        
        
     





}




Documento.prototype.getUrlFiltro = function( obj  ) {                
    
    var ret = "";    
      
    ret = obj.filtro;
    return ret;
};









Documento.prototype.icobusqueda = function( obj  ) {                
    
//fetch(  html.url.absolute() + '/com/busqueda/busqueda_bar.html' )

    //fetch( html.url.absolute() + '/aplicacion/documento/htmf/qry/busqueda_bar.html' )
    fetch(  html.url.absolute() + '/com/busqueda/busqueda_bar.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        document.getElementById( "bar_busqueda" ).innerHTML =  data;                    

        //html.topbar.busqueda_bar_accion(obj);
            
            var search_bar = document.getElementById( "icon-search_bar" );
            search_bar.addEventListener('click',
                function(event) {      


                    var b1 = document.getElementById( "icon-search_bar" ); 
                    b1.style.visibility = "hidden";        
                    b1.parentNode.style.display = "none";     

                    var b2 = document.getElementById( "icon-up_bar" ); 
                    b2.style.visibility = "visible";        
                    b2.parentNode.style.display = "block";     



                    // cuadro de busqueda            
                    //fetch( html.url.absolute() +  '/com/busqueda/busqueda.html' )
                    
                    
                    fetch( html.url.absolute() + '/aplicacion/documento/htmf/qry/busqueda.html' )
                      .then(response => {
                        return response.text();
                      })
                      .then(data => {
                        document.getElementById( "divbusqueda" ).innerHTML =  data;            

                        // cargar combo año
                        var domobj = document.getElementById("qry_agno");
                        
                        var today = new Date();
                        var year = today.getFullYear();
                        var firstyear = 2000;
                        var rango = year - firstyear ;


                        for( x=0; x < rango ; x++ ) {
                            var opt = document.createElement('option');                         
                            opt.value = year;      
                            opt.innerHTML = year;                                        
                            domobj.appendChild(opt);                     
                            year--;
                        }                           

                      
                      
                        var btn_busqueda = document.getElementById( "btn_busqueda" );                        
                        btn_busqueda.onclick = function(){                                  
                            
                            var tex = document.getElementById("qry_busquedatexto").value;
                            var aa = document.getElementById("qry_agno").value ;

                            obj.filtro = ';q=' + tex + ';aa=' +aa  ;

                            obj.main_list(obj, 1); 
                         
                        }
                        
                      
                      })            

         },
                false
            );     




            var up_bar = document.getElementById( "icon-up_bar" );
            up_bar.addEventListener('click',
                function(event) {      

                    var b1 = document.getElementById( "icon-search_bar" ); 
                    b1.style.visibility = "visible";        
                    b1.parentNode.style.display = "block";  

                    var b2 = document.getElementById( "icon-up_bar" ); 
                    b2.style.visibility = "hidden";        
                    b2.parentNode.style.display = "none";      

                    document.getElementById( "divbusqueda" ).innerHTML =  "";            

                    obj.filtro = "";    
                    obj.main_list(obj, 1); 

                },
                false
            );     
        
        

 
        
        
        
      })   
    
    
};






Documento.prototype.preedit = function( obj  ) {                
    
  /*
    var btn_file_cambiar = document.getElementById('btn_file_cambiar');
    btn_file_cambiar.style.display="block";
  */
  
};





